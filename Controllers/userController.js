const mongoose = require("mongoose");
const User = require("../Models/userSchema.js");


const bcrypt = require("bcrypt");
const auth = require("../auth.js");



/* CONTROLLERS */

// GetAllUsers Controller
	module.exports.getAllUsers = (request, response) => {

		User.find({})
		.then(result => {
			return response.send(result);
		})
		.catch(error => {
			return response.send(error);
		})
	};


// User Registration Controller
	module.exports.userRegistration = (request, response) => {
		const input = request.body;
		
		// check if email already exist
		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already existing!")
			} else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					// password: input.password,
					mobileNo: input.mobileNo
				});

				//to save newUser, use .save()
				newUser.save().then(save => {
					return response.send("The User is successfully added!")
				}).catch(error => {
					return response.send(error)
				})
			} // else email is null
		}).catch(error => {
			//error for findOne
			return response.send(error)
		})
	}


//S38-D1
// User Authentication Controller
module.exports.userAuthentication = (request, response) => {

	const input = request.body;

	/*
		Possible scenarios:
			1. Email is not yet registered
			2. Email is registered, but the password is wrong
	*/

	User.findOne({email: input.email})
	.then(result => {

		// Email registration check
		if(result === null){
			return response.send("Email is not yet registered. Please register before logging in.");
		} else {
			// we have to verify if the passwor is correct
			// The "compareSync" method is used to compare a non encrypted password to the encrypted password.
			//it returns boolean value, if match true value will return otherwise false.
			const isPasswordCorrect = bcrypt.compareSync(input.password,result.password);

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect.");
			}
		}
	}).catch(error => {
		return response.send(error);
		})
	};


//S38-A1
// getProfile Controller
	module.exports.getProfile = (request, response) => {
		// const input = request.body;
		const input = auth.decode(request.headers.authorization);
		console.log(input);


		// User.findbyId(input._id)
		User.findOne({ _id: input}) //also OK to use
		.then(result => {
			// if(result === null){
				// return response.send(`No user found with that ID. Please Enter another one.`)
			// } else{
				result.password = "";
				return response.send(result);
			// }
		}).catch(error => {
			return response.send(error);
		})
	};

// S39-D1
	// Retrieve the user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the result document to an empty string("").
			3. Return the result back to the frontend
		*/
	//getProfile Controller
	// module.exports.getProfile = (request, response) => {
	// 	const userData = auth.decode(request.headers.authorization);
	// 	console.log(userData);

	// 	User.findById(userData._id)
	// 	.then(result => {
	// 		//avoid to expose sensitive information such as password.
	// 		result.password = "";
	// 		return response.send(result);
	// 	})
	// }
