const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

// AddCourse Controller (for admin only)
module.exports.addCourse = (request, response) => {
	//S39-A1
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(userData.isAdmin === true){
		let input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// saves the created object to our database
		return newCourse.save()
		// course successfully created
		.then(course => {
			console.log(course);
			response.send(`Course Added!`);
		})
		// course creation failed
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	} else{
		return response.send(`You are not authorized to do this action!`)
	}
}


//create 
	/*
		{
		    "name": "HTML",
		    "description": "Learn the basics of Web Development",
		    "price": 1000
		}
	*/