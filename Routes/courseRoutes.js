const express = require("express");
const router = express.Router();

const courseController = require("../Controllers/courseController.js");
const auth = require("../auth.js");

//S39-D1
	//Route for creating a course
	// router.post("/", courseController.addCourse);

//S39-A1
//Route for creating a course (for admin only)
	router.post("/", auth.verify, courseController.addCourse);

module.exports = router;