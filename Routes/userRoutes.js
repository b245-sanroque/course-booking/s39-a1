const express = require("express");
const router = express.Router();

const userController = require("../Controllers/userController.js");

const auth = require("../auth.js");

/* ROUTES */

/* Route for getAllUsers (get) */
	router.get("/", userController.getAllUsers);

// Route for User Registration (post)
	router.post("/register", userController.userRegistration);

//s38-D1
// Route for User Authentication (post)
	router.post("/login", userController.userAuthentication);

//S38-A1
	// Route for getProfile (post)
	// router.post("/details", userController.getProfile);

//s39-d1
// Route for getProfile (post) > (get)
router.get("/details", auth.verify, userController.getProfile);


module.exports = router;
